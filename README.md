# O’Droid
Android theme with round icons, licensed under [CC BY](http://creativecommons.org/licenses/by/3.0/). Derived from [Numix Circle](https://github.com/numixproject/com.numix.icons_circle), which licensed icons under [CC BY](http://creativecommons.org/licenses/by/3.0/).

[![Build Status](https://travis-ci.org/mesnevi/odroid.svg?branch=master)](https://travis-ci.org/mesnevi/odroid) ![CI](https://github.com/mesnevi/odroid/workflows/CI/badge.svg)

## Prerequisites
You will need the folowing software to build this theme:

* [Android SDK](https://developer.android.com/sdk/)
* [Inkscape](https://inkscape.org)
* [make](https://www.gnu.org/software/make/)
* [Gradle](https://gradle.org/)

And those fonts:

* [Comfortaa](https://www.fontsquirrel.com/fonts/comfortaa)
* [DejaVu Sans](https://www.fontsquirrel.com/fonts/dejavu-sans)
* [Droid Sans](https://www.fontsquirrel.com/fonts/droid-sans)
* [Exo](https://www.fontsquirrel.com/fonts/exo)
* [URW Gothic L](http://fontsgeek.com/fonts/URW-Gothic-L-Book)

Icons that require fonts will look ugly without them!

## Build
In a terminal run:

	make -j `nproc`
	gradle build

## Install
Connect your [development-enabled](https://developer.android.com/tools/device.html#developer-device-options) Android device to your PC using a USB cable and run:

	gradle installDebug

On the gadget go to the launcher settings and select the theme.

## Add New Icon
To add a new app icon to this theme you need:

* [Package name](https://developer.android.com/guide/topics/manifest/manifest-element.html#package)
* [Activity name](https://developer.android.com/guide/topics/manifest/activity-element.html#nm)
* The icon itself (in the Inkscape SVG format)

Put the SVG file into `artwork/icons` directory and name it after the activity (replacing dots with underscores). You can omit several trailing components of the activity name, e.g. an icon for `com.example.foo.ui.MainActivity` may be named `com_example_foo.svg`.

Then add package name and activity name (separated by slash) line into `config` file. You can omit several leading components of the activity name if they coincide with the package name, e.g.: `com.example.foo/.ui.MainActivity`.

That's it. The build system will do the rest for you. And don't forget to make a pull request!

## Request New Icon
If drawing a new icon and adding it yourself doesn't strike you as a good deal, feel free to request it. Create new [issue](https://github.com/mesnevi/odroid/issues/new) with `icon request` label. If you're using an unusual device, please mention it:
![icon request](artwork/icon-request.png)

