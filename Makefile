densities = hdpi xhdpi xxhdpi xxxhdpi
outputs =

# Detect the proper export file name option for Inkscape. Run sequentially:
#   inkscape --export-filename foo.png --version # for Inkscape 1.1 and newer
#   inkscape --export-file foo.png --version     # for Inkscape 1.0
#   inkscape -e foo.png --version                # for Inkscape 0.92 and older
# The Inkscape's command line parser either fails on unknown option or prints
# the version string and succeeds. The successful form is assigned to a simply
# expanded variable (::=) to avoid evaluation in each rule that uses this
# variable.
inkscape-export ::= \
	$(foreach opt, --export-filename --export-file -e, \
		$(shell inkscape $(opt) foo.png --version 1> /dev/null 2> /dev/null \
				&& echo inkscape $(opt)))

include artwork/ic_launcher.mk
include artwork/icons/filter.mk
include artwork/icons/icons.mk

all : $(outputs)

clean :
	rm -f $(outputs)

distclean : clean
	rm -fR bin libs
	rm -f build.xml local.properties proguard-project.txt project.properties

.PHONY : all clean distclean
.DEFAULT_GOAL := all
