outputs += \
	$(foreach dpi, $(densities), \
		$(foreach prefix, assets/icons/res res, \
			$(addprefix $(prefix)/drawable-$(dpi)/, \
				$(patsubst %.svg,%.png, \
					$(notdir $(wildcard artwork/icons/*.svg))))))

assets/icons/res/drawable-hdpi/%.png : artwork/icons/%.svg
	$(inkscape-export) $@ -w 72 -h 72 $<

assets/icons/res/drawable-xhdpi/%.png : artwork/icons/%.svg
	$(inkscape-export) $@ -w 96 -h 96 $<

assets/icons/res/drawable-xxhdpi/%.png : artwork/icons/%.svg
	$(inkscape-export) $@ -w 144 -h 144 $<

assets/icons/res/drawable-xxxhdpi/%.png : artwork/icons/%.svg
	$(inkscape-export) $@ -w 192 -h 192 $<

res/drawable-hdpi/%.png : assets/icons/res/drawable-hdpi/%.png
	ln -s ../../assets/icons/$@ res/drawable-hdpi/

res/drawable-xhdpi/%.png : assets/icons/res/drawable-xhdpi/%.png
	ln -s ../../assets/icons/$@ res/drawable-xhdpi/

res/drawable-xxhdpi/%.png : assets/icons/res/drawable-xxhdpi/%.png
	ln -s ../../assets/icons/$@ res/drawable-xxhdpi/

res/drawable-xxxhdpi/%.png : assets/icons/res/drawable-xxxhdpi/%.png
	ln -s ../../assets/icons/$@ res/drawable-xxxhdpi/
