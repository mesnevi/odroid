outputs += assets/icons/res/xml/appfilter.xml res/xml/appfilter.xml

assets/icons/res/xml/appfilter.xml : config
	$(eval $@_TMP := $(shell mktemp))
	./generate < config > $($@_TMP)
	mv $($@_TMP) assets/icons/res/xml/appfilter.xml

res/xml/appfilter.xml : assets/icons/res/xml/appfilter.xml
	ln -sf ../../assets/icons/res/xml/appfilter.xml res/xml/appfilter.xml
