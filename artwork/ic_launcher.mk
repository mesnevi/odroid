outputs += $(foreach x, $(densities), res/drawable-$(x)/ic_launcher.png)

res/drawable-hdpi/ic_launcher.png : artwork/ic_launcher.svg
	$(inkscape-export) $@ -w 72 -h 72 $<

res/drawable-xhdpi/ic_launcher.png : artwork/ic_launcher.svg
	$(inkscape-export) $@ -w 96 -h 96 $<

res/drawable-xxhdpi/ic_launcher.png : artwork/ic_launcher.svg
	$(inkscape-export) $@ -w 144 -h 144 $<

res/drawable-xxxhdpi/ic_launcher.png : artwork/ic_launcher.svg
	$(inkscape-export) $@ -w 192 -h 192 $<
